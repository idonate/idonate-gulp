import gClean from 'gulp-clean';

export function Clean(gulp) {
    return function() {
        return gulp.src(['.tmp', 'dist'], {read: false})
        .pipe(gClean());
    }
}
