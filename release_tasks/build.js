import gIf from 'gulp-if';
import gInject from 'gulp-inject';
import gUseref from 'gulp-useref';
import gBless from 'gulp-bless';
import cachebust from 'gulp-cache-bust';
import gUglify from 'gulp-uglify-es';

export function Build(gulp, opts) {
    return function() {
        return gulp.src(opts.app+'/index.html')
        .pipe(gInject(gulp.src(['.tmp/*.js', '.tmp/**/*.module.js', '.tmp/**/*-module.js', '.tmp/**/*.{js,*.js,css,*.css}']),{ignorePath:'.tmp'}))
        .pipe(gulp.dest('.tmp'))
        .pipe(gIf(opts.deploy, gUseref()))
        .pipe(gIf(opts.deploy, gIf('*.js', gUglify({mangle:false}))))
        //.pipe(gIf(opts.deploy, gIf('*.css', gBless())))
        .pipe(gIf(opts.deploy, gulp.dest('dist')))
        .pipe(gIf(opts.deploy, gIf('*.html', cachebust({type: 'timestamp'}))))
        .pipe(gIf(opts.deploy, gulp.dest('dist')));
    }
}
