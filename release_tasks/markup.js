import lazypipe from 'lazypipe';
import gHtmlmin from 'gulp-htmlmin';
import gAngularTemplatecache from 'gulp-angular-templatecache';

export function Markup(gulp, opts) {
    return function() {
        return gulp.src(`${opts.app}/**/*.html`)
        .pipe(MarkupLib(opts)())
        .pipe(gulp.dest('.tmp/scripts'));
    }
}

export function MarkupLib(opts) {
    return lazypipe()
    .pipe(() => gHtmlmin({collapseWhitespace: true}))
    .pipe(() => gAngularTemplatecache('templates.js', {standalone: true}));
}
