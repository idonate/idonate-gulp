import lazypipe from 'lazypipe';
import gCoffee from 'gulp-coffee';
import gIf from 'gulp-if';
import gNgAnnotate from 'gulp-ng-annotate';
import gUglify from 'gulp-uglify-es';
import gSourceMaps from 'gulp-sourcemaps';
import cachebust from 'gulp-cache-bust';

export function Scripts(gulp, opts, $) {
  return function() {
    gulp.src(`${opts.app}/**/*.{coffee,js}`)
    .pipe(ScriptsLib(opts, $)())
    .pipe(gulp.dest('.tmp'));
  };
}

export function ScriptsLib(opts, $) {
  return lazypipe()
  .pipe(() => gIf(!opts.deploy, gSourceMaps.init()))
  .pipe(() => gIf('*.coffee', gCoffee()))
  .pipe(() => gIf(!opts.skipAnnotate, gNgAnnotate()))
  .pipe(() => gIf(!opts.deploy, gSourceMaps.write()));
}
