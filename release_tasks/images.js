import gCache from 'gulp-cache';
import gIf from 'gulp-if';
import gImagemin from 'gulp-imagemin';

export function Images(gulp, opts) {
    return function() {
        return gulp.src([opts.app+'/images/**/*',
            'bower_components/**/images/*'])
        .pipe(gCache(gImagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{cleanupIDs: false}]
        })))
        .pipe(gulp.dest('.tmp/images'))
        .pipe(gIf(opts.deploy, gulp.dest('dist/images')));
    }
}
