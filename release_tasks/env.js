import gRename from 'gulp-rename';

export function Env(gulp, opts) {
    var env;
    if(opts.production)
        env = 'production.js';
    else if(opts.staging)
        env = 'staging.js';
    else
        env = 'development.js';
    return function() {
        return gulp.src(`env_vars/${env}`)
        .pipe(gRename('env_vars.js'))
        .pipe(gulp.dest('.tmp/scripts'));
    }
}
