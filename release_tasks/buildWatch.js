import gIf from 'gulp-if';
import gInject from 'gulp-inject';
import gUseref from 'gulp-useref';
import gBless from 'gulp-bless';
import browserSync from 'browser-sync';

export function BuildWatch(gulp) {
    return function() {
        browserSync.reload();
    }
}
