export function Copy(gulp, opts) {
    var src = [
        `${opts.app}/{*,**/*}`,
        `!${opts.app}/*.{coffee,js,html,sass,css}`
    ];
    return function CopyTask() {
        return gulp.src(src, {base:opts.app,dot:true})
        .pipe(gulp.dest('.tmp'));
    }
}
