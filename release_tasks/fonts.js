import mainBowerFiles from 'main-bower-files';

export function Fonts(gulp, opts) {
    return function() {
        return gulp.src(mainBowerFiles(
            '**/*.{eot,svg,ttf,woff,woff2}',
            function (err) {}
        )
        .concat(`${opts.app}/fonts/**/*`))
        .pipe(gulp.dest('.tmp/fonts'));
    }
}
