import lazypipe from 'lazypipe';
import gSass from 'gulp-sass';
import gIf from 'gulp-if';
import gUglifycss from 'gulp-uglifycss';
import gAutoprefixer from 'gulp-autoprefixer';

export function Styles(gulp, opts) {
    return function() {
        gulp.src(`${opts.app}/**/*.{scss,css}`)
        .pipe(StylesLib(opts)())
        .pipe(gulp.dest('.tmp'));
    }
}

export function StylesLib(opts) {
    return lazypipe()
    .pipe(gSass)
    .pipe(gAutoprefixer)
    .pipe(() => gIf(opts.deploy, gUglifycss()));
}
