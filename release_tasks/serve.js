import * as browserSync from 'browser-sync';
import modRewrite from 'connect-modrewrite';

export function Serve(gulp, opts) {
    var dir = '.tmp';
    if(opts.deploy)
        dir = 'dist';
    return function() {
        browserSync.init({
            notify: false,
            port: 9000,
            server: {
                baseDir: dir,
                routes: {'/node_modules': 'node_modules'},
                middleware: [
                    modRewrite([
                        '^/data/(.*) - [L]',
                        '!\\.\\w+$ /index.html [L]'
                    ])
                ]
            }
        });
        gulp.watch(`${opts.app}/**/*.{scss,css,coffee,js,html}`, ['buildWatch']);
    }
}
