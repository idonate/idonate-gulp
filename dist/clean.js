'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Clean = Clean;

var _gulpClean = require('gulp-clean');

var _gulpClean2 = _interopRequireDefault(_gulpClean);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Clean(gulp) {
    return function () {
        return gulp.src(['.tmp', 'dist'], { read: false }).pipe((0, _gulpClean2.default)());
    };
}