'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Fonts = Fonts;

var _mainBowerFiles = require('main-bower-files');

var _mainBowerFiles2 = _interopRequireDefault(_mainBowerFiles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Fonts(gulp, opts) {
    return function () {
        return gulp.src((0, _mainBowerFiles2.default)('**/*.{eot,svg,ttf,woff,woff2}', function (err) {}).concat(opts.app + '/fonts/**/*')).pipe(gulp.dest('.tmp/fonts'));
    };
}