'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Serve = Serve;

var _browserSync = require('browser-sync');

var browserSync = _interopRequireWildcard(_browserSync);

var _connectModrewrite = require('connect-modrewrite');

var _connectModrewrite2 = _interopRequireDefault(_connectModrewrite);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function Serve(gulp, opts) {
    var dir = '.tmp';
    if (opts.deploy) dir = 'dist';
    return function () {
        browserSync.init({
            notify: false,
            port: 9000,
            server: {
                baseDir: dir,
                routes: { '/node_modules': 'node_modules' },
                middleware: [(0, _connectModrewrite2.default)(['^/data/(.*) - [L]', '!\\.\\w+$ /index.html [L]'])]
            }
        });
        gulp.watch(opts.app + '/**/*.{scss,css,coffee,js,html}', ['buildWatch']);
    };
}