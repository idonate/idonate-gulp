'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Build = Build;

var _gulpIf = require('gulp-if');

var _gulpIf2 = _interopRequireDefault(_gulpIf);

var _gulpInject = require('gulp-inject');

var _gulpInject2 = _interopRequireDefault(_gulpInject);

var _gulpUseref = require('gulp-useref');

var _gulpUseref2 = _interopRequireDefault(_gulpUseref);

var _gulpBless = require('gulp-bless');

var _gulpBless2 = _interopRequireDefault(_gulpBless);

var _gulpCacheBust = require('gulp-cache-bust');

var _gulpCacheBust2 = _interopRequireDefault(_gulpCacheBust);

var _gulpUglifyEs = require('gulp-uglify-es');

var _gulpUglifyEs2 = _interopRequireDefault(_gulpUglifyEs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Build(gulp, opts) {
    return function () {
        return gulp.src(opts.app + '/index.html').pipe((0, _gulpInject2.default)(gulp.src(['.tmp/*.js', '.tmp/**/*.module.js', '.tmp/**/*-module.js', '.tmp/**/*.{js,*.js,css,*.css}']), { ignorePath: '.tmp' })).pipe(gulp.dest('.tmp')).pipe((0, _gulpIf2.default)(opts.deploy, (0, _gulpUseref2.default)())).pipe((0, _gulpIf2.default)(opts.deploy, (0, _gulpIf2.default)('*.js', (0, _gulpUglifyEs2.default)({ mangle: false }))))
        //.pipe(gIf(opts.deploy, gIf('*.css', gBless())))
        .pipe((0, _gulpIf2.default)(opts.deploy, gulp.dest('dist'))).pipe((0, _gulpIf2.default)(opts.deploy, (0, _gulpIf2.default)('*.html', (0, _gulpCacheBust2.default)({ type: 'timestamp' })))).pipe((0, _gulpIf2.default)(opts.deploy, gulp.dest('dist')));
    };
}