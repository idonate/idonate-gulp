'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Copy = Copy;
function Copy(gulp, opts) {
    var src = [opts.app + '/{*,**/*}', '!' + opts.app + '/*.{coffee,js,html,sass,css}'];
    return function CopyTask() {
        return gulp.src(src, { base: opts.app, dot: true }).pipe(gulp.dest('.tmp'));
    };
}