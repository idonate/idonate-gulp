'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Styles = Styles;
exports.StylesLib = StylesLib;

var _lazypipe = require('lazypipe');

var _lazypipe2 = _interopRequireDefault(_lazypipe);

var _gulpSass = require('gulp-sass');

var _gulpSass2 = _interopRequireDefault(_gulpSass);

var _gulpIf = require('gulp-if');

var _gulpIf2 = _interopRequireDefault(_gulpIf);

var _gulpUglifycss = require('gulp-uglifycss');

var _gulpUglifycss2 = _interopRequireDefault(_gulpUglifycss);

var _gulpAutoprefixer = require('gulp-autoprefixer');

var _gulpAutoprefixer2 = _interopRequireDefault(_gulpAutoprefixer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Styles(gulp, opts) {
    return function () {
        gulp.src(opts.app + '/**/*.{scss,css}').pipe(StylesLib(opts)()).pipe(gulp.dest('.tmp'));
    };
}

function StylesLib(opts) {
    return (0, _lazypipe2.default)().pipe(_gulpSass2.default).pipe(_gulpAutoprefixer2.default).pipe(function () {
        return (0, _gulpIf2.default)(opts.deploy, (0, _gulpUglifycss2.default)());
    });
}