'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.BuildWatch = BuildWatch;

var _gulpIf = require('gulp-if');

var _gulpIf2 = _interopRequireDefault(_gulpIf);

var _gulpInject = require('gulp-inject');

var _gulpInject2 = _interopRequireDefault(_gulpInject);

var _gulpUseref = require('gulp-useref');

var _gulpUseref2 = _interopRequireDefault(_gulpUseref);

var _gulpBless = require('gulp-bless');

var _gulpBless2 = _interopRequireDefault(_gulpBless);

var _browserSync = require('browser-sync');

var _browserSync2 = _interopRequireDefault(_browserSync);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function BuildWatch(gulp) {
    return function () {
        _browserSync2.default.reload();
    };
}