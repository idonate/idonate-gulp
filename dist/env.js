'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Env = Env;

var _gulpRename = require('gulp-rename');

var _gulpRename2 = _interopRequireDefault(_gulpRename);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Env(gulp, opts) {
    var env;
    if (opts.production) env = 'production.js';else if (opts.staging) env = 'staging.js';else env = 'development.js';
    return function () {
        return gulp.src('env_vars/' + env).pipe((0, _gulpRename2.default)('env_vars.js')).pipe(gulp.dest('.tmp/scripts'));
    };
}