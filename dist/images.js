'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Images = Images;

var _gulpCache = require('gulp-cache');

var _gulpCache2 = _interopRequireDefault(_gulpCache);

var _gulpIf = require('gulp-if');

var _gulpIf2 = _interopRequireDefault(_gulpIf);

var _gulpImagemin = require('gulp-imagemin');

var _gulpImagemin2 = _interopRequireDefault(_gulpImagemin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Images(gulp, opts) {
    return function () {
        return gulp.src([opts.app + '/images/**/*', 'bower_components/**/images/*']).pipe((0, _gulpCache2.default)((0, _gulpImagemin2.default)({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{ cleanupIDs: false }]
        }))).pipe(gulp.dest('.tmp/images')).pipe((0, _gulpIf2.default)(opts.deploy, gulp.dest('dist/images')));
    };
}