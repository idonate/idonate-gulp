'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Scripts = Scripts;
exports.ScriptsLib = ScriptsLib;

var _lazypipe = require('lazypipe');

var _lazypipe2 = _interopRequireDefault(_lazypipe);

var _gulpCoffee = require('gulp-coffee');

var _gulpCoffee2 = _interopRequireDefault(_gulpCoffee);

var _gulpIf = require('gulp-if');

var _gulpIf2 = _interopRequireDefault(_gulpIf);

var _gulpNgAnnotate = require('gulp-ng-annotate');

var _gulpNgAnnotate2 = _interopRequireDefault(_gulpNgAnnotate);

var _gulpUglifyEs = require('gulp-uglify-es');

var _gulpUglifyEs2 = _interopRequireDefault(_gulpUglifyEs);

var _gulpSourcemaps = require('gulp-sourcemaps');

var _gulpSourcemaps2 = _interopRequireDefault(_gulpSourcemaps);

var _gulpCacheBust = require('gulp-cache-bust');

var _gulpCacheBust2 = _interopRequireDefault(_gulpCacheBust);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Scripts(gulp, opts, $) {
  return function () {
    gulp.src(opts.app + '/**/*.{coffee,js}').pipe(ScriptsLib(opts, $)()).pipe(gulp.dest('.tmp'));
  };
}

function ScriptsLib(opts, $) {
  return (0, _lazypipe2.default)().pipe(function () {
    return (0, _gulpIf2.default)(!opts.deploy, _gulpSourcemaps2.default.init());
  }).pipe(function () {
    return (0, _gulpIf2.default)('*.coffee', (0, _gulpCoffee2.default)());
  }).pipe(function () {
    return (0, _gulpIf2.default)(!opts.skipAnnotate, (0, _gulpNgAnnotate2.default)());
  }).pipe(function () {
    return (0, _gulpIf2.default)(!opts.deploy, _gulpSourcemaps2.default.write());
  });
}