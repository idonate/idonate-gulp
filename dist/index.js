'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _build = require('./build');

Object.defineProperty(exports, 'Build', {
  enumerable: true,
  get: function get() {
    return _build.Build;
  }
});

var _buildWatch = require('./buildWatch');

Object.defineProperty(exports, 'BuildWatch', {
  enumerable: true,
  get: function get() {
    return _buildWatch.BuildWatch;
  }
});

var _clean = require('./clean');

Object.defineProperty(exports, 'Clean', {
  enumerable: true,
  get: function get() {
    return _clean.Clean;
  }
});

var _copy = require('./copy');

Object.defineProperty(exports, 'Copy', {
  enumerable: true,
  get: function get() {
    return _copy.Copy;
  }
});

var _env = require('./env');

Object.defineProperty(exports, 'Env', {
  enumerable: true,
  get: function get() {
    return _env.Env;
  }
});

var _fonts = require('./fonts');

Object.defineProperty(exports, 'Fonts', {
  enumerable: true,
  get: function get() {
    return _fonts.Fonts;
  }
});

var _images = require('./images');

Object.defineProperty(exports, 'Images', {
  enumerable: true,
  get: function get() {
    return _images.Images;
  }
});

var _markup = require('./markup');

Object.defineProperty(exports, 'Markup', {
  enumerable: true,
  get: function get() {
    return _markup.Markup;
  }
});

var _scripts = require('./scripts');

Object.defineProperty(exports, 'Scripts', {
  enumerable: true,
  get: function get() {
    return _scripts.Scripts;
  }
});

var _serve = require('./serve');

Object.defineProperty(exports, 'Serve', {
  enumerable: true,
  get: function get() {
    return _serve.Serve;
  }
});

var _styles = require('./styles');

Object.defineProperty(exports, 'Styles', {
  enumerable: true,
  get: function get() {
    return _styles.Styles;
  }
});