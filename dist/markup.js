'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Markup = Markup;
exports.MarkupLib = MarkupLib;

var _lazypipe = require('lazypipe');

var _lazypipe2 = _interopRequireDefault(_lazypipe);

var _gulpHtmlmin = require('gulp-htmlmin');

var _gulpHtmlmin2 = _interopRequireDefault(_gulpHtmlmin);

var _gulpAngularTemplatecache = require('gulp-angular-templatecache');

var _gulpAngularTemplatecache2 = _interopRequireDefault(_gulpAngularTemplatecache);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Markup(gulp, opts) {
    return function () {
        return gulp.src(opts.app + '/**/*.html').pipe(MarkupLib(opts)()).pipe(gulp.dest('.tmp/scripts'));
    };
}

function MarkupLib(opts) {
    return (0, _lazypipe2.default)().pipe(function () {
        return (0, _gulpHtmlmin2.default)({ collapseWhitespace: true });
    }).pipe(function () {
        return (0, _gulpAngularTemplatecache2.default)('templates.js', { standalone: true });
    });
}