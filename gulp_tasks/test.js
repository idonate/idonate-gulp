export function Test(gulp, opts, $) {
    return function() {
        return gulp.src('tests/*')
        .pipe($.ava({verbose: true}));
    };
}