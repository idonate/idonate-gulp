export function Build(gulp, opts, $) {
    return function() {
        gulp.src(['release_tasks/*.js'])
        .pipe($.babel())
        .pipe(gulp.dest('dist'));
    };
}