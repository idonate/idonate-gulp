import gulp from 'gulp';
import gulpHelp from 'gulp-help';
import {Build, Test} from './gulp_tasks';
import loadPlugins from 'gulp-load-plugins';

const finalGulp = gulpHelp(gulp);
const options = {};
const plugins = loadPlugins();

finalGulp
.task('build', 'Package the gulp tasks', Build(finalGulp, options, plugins))
.task('test', 'Test the gulp tasks', Test(finalGulp, options, plugins))
.task('default', 'Runs the build task', ['build']);
