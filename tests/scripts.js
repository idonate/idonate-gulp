import test from 'ava';
import loadPlugins from 'gulp-load-plugins';
import {FromString} from './helpers/pipe';
import {ScriptsLib} from '../release_tasks/scripts';
const plugins = loadPlugins();


test('scripts', t => {
  const expected = `(function() {
  var TestCtrl;

  angular.module('test').component('testCmp', {
    template: '<div></div>',
    bindings: {
      test: '='
    },
    controller: TestCtrl = (function() {
      function TestCtrl($scope) {
        this.someMethod();
      }

      TestCtrl.prototype.someMethod = function() {};

      return TestCtrl;

    })()
  });

}).call(this);
`
  const input = `angular.module 'test'
.component 'testCmp',
  template: '<div></div>'
  bindings:
    test: '='
  controller: class TestCtrl
    constructor: ($scope) ->
      @someMethod()
    someMethod: ->
      return`

  return FromString(input, 'test.coffee', () => {
    return ScriptsLib({}, plugins)();
  })
  .then(output => {
    t.is(output.contents.toString(), expected.toString(), 'Compiled as expected');
  }, error => {
    t.fail(error);
  });  
});

test('scripts-production', t => {
  const expected = `(function(){var TestCtrl;angular.module(\"test\").component(\"testCmp\",{template:\"<div></div>\",bindings:{test:\"=\"},controller:TestCtrl=function(){function TestCtrl($scope){this.someMethod()}return TestCtrl.prototype.someMethod=function(){},TestCtrl}()})}).call(this);`
  const input = `angular.module 'test'
.component 'testCmp',
  template: '<div></div>'
  bindings:
    test: '='
  controller: class TestCtrl
    constructor: ($scope) ->
      @someMethod()
    someMethod: ->
      return`

  return FromString(input, 'test.coffee', () => {
    return ScriptsLib({production: true}, plugins)();
  })
  .then(output => {
    t.is(output.contents.toString(), expected.toString(), 'Compiled as expected');
  }, error => {
    t.fail(error);
  });  
});
