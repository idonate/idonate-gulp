import vs from 'vinyl-string';
import vfs from 'vinyl-fs';
import map from 'map-stream';

export function FromString(input, path, func) {
  return new Promise((res, rej) => {
    let contents = '';

    const vFile = vs(input, {
      path,
    });

    vFile
      .pipe(func())
      .on('error', e => {
        rej(e);
      })
      .pipe(map((file, cb) => {
        contents = file;
        cb(null, file);
      }))
      .on('end', () => {
        res(contents);
      });
  });
};

export function FromPath(input, func) {
  return new Promise((res, rej) => {
    let contents = '';

    vfs.src(input)
      .pipe(func())
      .on('error', e => {
        rej(e);
      })
      .pipe(map((file, cb) => {
        contents = file;
        cb(null, file);
      }))
      .on('end', () => {
        res(contents);
      });
  });
};
