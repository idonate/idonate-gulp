import gulp from 'gulp';
import help from 'gulp-help';
import gUtil from 'gulp-util';
import * as tasks from 'idonate-gulp'

const gulpHelp = help(gulp);
const opts = {
    app: gUtil.env.app || 'app',
    production: gUtil.env.production,
    staging: gUtil.env.staging,
    deploy: gUtil.env.deploy
}

gulpHelp
.task('build', ['copy', 'env', 'fonts', 'images', 'markup', 'scripts', 'styles'], tasks.Build(gulpHelp, opts))
.task('buildWatch', ['build'], tasks.BuildWatch(gulpHelp))
.task('clean', tasks.Clean(gulpHelp))
.task('copy', tasks.Copy(gulpHelp, opts))
.task('env', tasks.Env(gulpHelp, opts))
.task('fonts', tasks.Fonts(gulpHelp, opts))
.task('images', tasks.Images(gulpHelp, opts))
.task('markup', tasks.Markup(gulpHelp, opts))
.task('scripts', tasks.Scripts(gulpHelp, opts))
.task('serve', ['build'], tasks.Serve(gulpHelp, opts))
.task('styles', tasks.Styles(gulpHelp, opts));
