# iDonate Angular Gulpfile

This repo contains the shared Gulp configuration for all the iDonate Angular apps.

It recommends the following project structure:

    project
    |--> README.md
    |--> package.json
    |--> gulpfile.js
    |--> node_modules/
    |--> env_vars/
    |--> app/
        |--> index.html
        |--> favicon.ico
        |--> styles/
        |--> images/
        |--> scripts/
            |--> app.config
            |--> app.run
            |--> etc...

It includes the following tasks:

* Build
    - Builds the entire app. If a *production* arg is given, builds to *dist* folder.
    Otherwise builds to *.tmp* folder.
* Clean
    - Removes all built files from *.tmp* and *dist*.
* Copy
    - Copys all non-buildable files (images, etc.) to build folder.
* Default
    - Prints help message
* Env
    - Copys proper environment variables to build folder. If passed *production* or
    *staging*, copies corresponding vars. Otherwise, copys *dev* vars.
* Markup
    - Builds all markup files and copies them to build folder.
* Scripts
    - Builds all script files and copies them to build folder.
* Serve
    - Serves app from *.tmp* (or *dist*, if *production* arg is given) folder.
* Styles
    - Builds all styling files and copies them to build folder.


